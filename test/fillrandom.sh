bench_path="/home/zhouhao/leveldb/build/db_bench"
db_path="/OPTANE/zh/leveldb_test"

#key_size="16" (default)
value_size="512"
benchmarks="fillrandom,stats"
num="30000000"
threads="12"
histogram="1"

RUN_ONE_TEST() {
  const_params="
    --db=$db_path \
    --benchmarks=$benchmarks \
    --histogram=$histogram \
    --num=$num \
    --value_size=$value_size\
  "
  echo "zhouhao9021" | sudo -S bash -c "echo 3 > /proc/sys/vm/drop_caches"
  cmd="nohup $bench_path $const_params >> demo.out 2>&1 &"
  echo $cmd > demo.out 
  echo $cmd
  eval $cmd
}

RUN_ONE_TEST
